![cmsship](https://cmsship.com/img/color-logo.png "cmsship")

## Drupal 7 & 8 Plugin

### Directions

Install this plugin (via drush or download and manually install), add your site to cmsship.com, copy the Site Key which is provided at cmsship.com for your site and then visit the settings area in Configuration -> cmsship and enter your Site Key and username.

Enjoy!
